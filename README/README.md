## INTRODUCERE

Sportul in ziua de astazi reprezinta una dintre cele placute activitati de catre toti oamenii. Plecand de la sporturi precum hockey sau gimnastica si ajungand pana la fotbal si baschet, sportul creeaza in aceste timpuri o audienta uriasa pe intreg pamantul. Asadar, avand in vedere acest lucru, am decis ca pentru acest proiect sa dezvolt o mica platforma in care fanii sportului, in special al fotbalului pot cauta anumite detalii despre echipe si ligi pe care acestiea le indragesc.


## Descirere problema

Dupa cum se cunoaste, dintre toate sporturile fotbalul realizeaza una dintre cele mai mari, daca nu chiar cea mai mare, audienta worldwide acesta fiind on sport practicat la nivel mondial. Asadar, acest lucru genreaza automat o multitudine de fani care doresc mereu sa fie la zi cu toate informatiile legate de acest sport, acest lucru putand fi facut cu o multitudine de aplicatii. Pentru acest proiect am dezvoltat o aplicatie care poate oferi utilizatorilor date despre anumite echipe pe care acestia le cauta si date despre anumite ligi din anumite tari, mai exact clasamentul si echipele din acea liga. Acest lucru a putut fi realizat utilizand doua API-uuri care contin date despre echipe si ligi de pe intreg globul. Aceste doua API-uri sunt TheSportsDB si AllSportsApi.


## Descriere API.

Dupa cum a fost specificat mai sus, au fost folosite doua API-uri destul de cunoscute, amandoua avand date despre lumea sportului. API-ul TheSportsDB contine date despre toate tipurile de sporturi existente, plecand de la echipe si ligi, pana la jucatori si contractele acestora din anumite sezoane. In mare parte functionalitatile acestui api sunt gratuite, cele putin mai simple, cele mai avansate cu detalii mai multe necesitand o donatie pe patreon. Totodata, acesta contine API-uri si pentru anumite imapini importante/ hihglighturi ale anumitor meciuri. Acesta linkuri pot fi foloste foarte usor prin intermediul unui fetch nefiind necesare autintificarea sau folosirea unui apiKey (pentru cele gratuite).

Cel de al doilea API, AllSportApi, fata de cel din urma neceista inregistrarea unui cont pe platforma acestora pentru a genera un api key care in final sa fie folosit de catre utilizator in procesul de accesare a datelor. Acesta contine date despre lumea sportului, aproape la fel de detaliate precum pe TheSportsDB. Diferit, acesta contine date despre pariuri si livesscoruri pentru anumite meciuri. Fata de apiul anterior acesta nu necesita platirea unei sume de bani pentru a folosi diferitele functionalitati.


## Flux de date

Aplicatia este formata din doua parti.
Prima parte cea care apeleaza un url de pe TheSportsDB este formata dintr-un input, care primeste ca date numele unei echipe iar la apasarea butonului cauta acesta returneaza un tabel cu toate echipele la nivel mondial care contin acel nume tastat de catre utilizator.
Exemple: Arsenal, Barcelona, Madrid, Dinamo Bucuresti, Farul etc.

Cea de a doua parte la randul ei este formata tot dintr-un tabel si doua inputuri. Cele doua inputuri primesc date precum numele tarii si al ligii. Aici dupa apasarea butonului cauta, aplicatia v-a prezenta clasamentul ligii respective din ziua respectiva. Pentru a reusi acest lucru mai intai este folosita o metoda GET care gaseste liga specificata de catre utilizator iar mai apo este folosit un alt GET care din vectorul generat ia datele TOTALE din sezonul respectiv pana la acel moment al apelarii(clasamentele sunt afisate sub forma de 3 tipuri: away, home, total). Dupa cum a fost specificat mai sus pentru a folosi aceste metode, a fost necesara inregistrarea pe platforma pentru a folosi api key-ul generat.

Exemple: France/ Ligue 1, Germany/Bundesliga (datele introduse trebuie sa fie in limba engleza).


## Capturi de ecran:

![Scheme](home.png)
![Scheme](call1.png)
![Scheme](call2.png)







